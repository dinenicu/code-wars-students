package com.hp.services;


import com.hp.entity.Product;
import com.hp.utils.Util;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductsService {
    private final static Logger LOGGER = Logger.getLogger(ProductsService.class.getName());
    private static ProductsService instance;

    private Random randomGenerator;
    private ImagesService imagesService;
    private Map<Integer, Product> productsById;

    private ProductsService() {
        imagesService = ImagesService.getInstance();
        productsById = new HashMap<>();
        randomGenerator = new Random();
        loadProducts();
    }

    public static ProductsService getInstance() {
        if (instance == null) {
            instance = new ProductsService();
        }
        return instance;
    }

    public List<Product> getAllProducts() {
        return new ArrayList<>(productsById.values());
    }

    public Product getProductById(int id) {
        return productsById.get(id);
    }

    public Product addProduct(Product product) {
        product.setId(productsById.size() + 1);
        productsById.put(product.getId(), product);

        return product;
    }

    public boolean updateProduct(int id, Product newProduct) {
        if (productsById.get(id) == null) {
            return false;
        }

        productsById.put(id, newProduct);

        return true;
    }

    public boolean removeProduct(int id) {
        if (productsById.get(id) == null) {
            return false;
        }
        try {
            imagesService.deleteImageById(id);
            productsById.remove(id);
        } catch (URISyntaxException | IOException e) {
            return false;
        }
        return true;
    }

    public Product searchProduct() {
        int index = randomGenerator.nextInt(productsById.size());
        return getProductById(index);
    }

    private void loadProducts() {
        Workbook workbook = null;
        try {
            workbook = Workbook.getWorkbook(new File(Util.getPath("products/products.xls").toString()));
            Sheet sheet = workbook.getSheet(0);
            int row = 0;
            try {
                while (1 == 1) {
                    row++;
                    Product product = new Product();
                    product.setId(row);
                    product.setProvider(sheet.getCell(0, row).getContents());
                    product.setName(sheet.getCell(1, row).getContents());

                    productsById.put(row, product);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                // done
            }
        } catch (IOException | BiffException | URISyntaxException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
    }
}
