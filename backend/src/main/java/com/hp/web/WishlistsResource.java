package com.hp.web;

import com.hp.entity.Wishlist;
import com.hp.services.UsersService;
import com.hp.services.WishlistService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Singleton
@Path("/wishlists")
@Api(value = "Wishlists")
public class WishlistsResource {

    private UsersService usersService = new UsersService();
    private WishlistService wishlistService = WishlistService.getInstance();

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get all public wishlists", response = Wishlist.class)
    public Response getAllPublicWishlists() {
        List<Wishlist> wishlists = wishlistService.getAllPublicWishlist();
        return Response.ok(wishlists).build();
    }

    @GET
    @Path("/{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get all wishlists for the user", response = Wishlist.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "User does not have any wishlists"),
            @ApiResponse(code = 404, message = "There is no user with the given id")
    })
    public Response getUserWishlists(@PathParam("userId") int userId) {
        List<Wishlist> wishlists = usersService.getUserWishlists(userId);

        if (usersService.getUserById(userId) == null) {
            return Response.status(404).build();
        } else if (wishlists.size() == 0) {
            return Response.status(204).build();
        }
        return Response.ok(wishlists).build();
    }

    @POST
    @Path("/{userId}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Add new wishlist for user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no user with the given id")
    })
    public javax.ws.rs.core.Response addUserWishlist(@PathParam("userId") int userId,
                                                     @ApiParam(name = "Wishlist", value = "The new wishlist", required = true)
                                                     Wishlist wishlist) {
        if (usersService.getUserById(userId) == null) {
            return Response.status(404).build();
        }

        return Response.status(200).entity(usersService.addUserWishlist(userId, wishlist)).build();
    }

    @DELETE
    @Path("/{userId}/{wishlistId}")
    @ApiOperation(value = "Remove an existing wishlist for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no user or wishlist with the given id")
    })
    public Response removeUserWishlist(@PathParam("userId") int userId, @PathParam("wishlistId") int wishlistId) {
        if (usersService.getUserById(userId) == null || !usersService.removeUserWishlist(userId, wishlistId)) {
            return Response.status(404).build();
        }

        return Response.status(200).build();
    }

    @PUT
    @Path("/{userId}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update an existing wishlist for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no user with the given id")
    })
    public Response updateUserWishlist(@PathParam("userId") int userId,
                                       @ApiParam(name = "Wishlist", value = "The updated wishlist", required = true)
                                       Wishlist wishlist) {
        if (usersService.getUserById(userId) == null) {
            return Response.status(404).build();
        }
        usersService.updateUserWishlist(userId, wishlist);

        return Response.status(200).build();
    }
}
