package com.hp.web;

import com.hp.entity.Product;
import com.hp.services.ImagesService;
import com.hp.services.ProductsService;
import com.hp.utils.Util;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.List;

@Singleton
@Path("/products")
@Api(value = "Products")
public class ProductsResource {

    private ProductsService productsService = ProductsService.getInstance();
    private ImagesService imagesService = ImagesService.getInstance();

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get all products", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "There are no products")
    })
    public Response getAllProducts() {
        List<Product> products = productsService.getAllProducts();
        if (products.size() == 0) {
            return Response.status(204).build();
        }
        return Response.ok(products).build();
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get product by id", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no product with the given id")
    })
    public Response getProductById(@PathParam("id") int id) {
        Product product = productsService.getProductById(id);
        if (product == null) {
            return Response.status(404).build();
        }
        return Response.ok(product).build();
    }

    @GET
    @Path("/images/{productId}")
    @Produces({"image/png", "image/jpeg", "image/gif"})
    @ApiOperation(value = "Get corresponding image of the product")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no image for the given product id")
    })
    public Response getProductImage(@PathParam("productId") int productId) {
        byte[] bytes;
        try {
            bytes = IOUtils.toByteArray(
                    new FileInputStream(Util.getPath("products/images/" + imagesService.findImageNameById(productId)).toString()));
        } catch (URISyntaxException | IOException e) {
            return Response.status(404).build();
        }
        if (bytes == null) {
            return Response.status(404).build();
        }
        return Response.ok(Base64.getEncoder().encode(bytes)).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Add new product")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Image could not be saved")
    })
    public Response addProduct(
            @ApiParam(name = "Product", value = "The new product", required = true) Product product) {
        Product addedProduct = productsService.addProduct(product);

        return Response.status(200).entity(addedProduct).build();
    }

    @POST
    @Path("/images/{productId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Upload product image")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Image could not be saved")
    })
    public Response uploadProductImage(
            @PathParam("productId") int productId,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        try {
            if (productsService.getProductById(productId) != null) {
                // product already has an image
                // delete existing one
                imagesService.deleteImageById(productId);
            }
            if (!imagesService.writeToFile(uploadedInputStream,
                    Util.getPath("products/images/").toString() + "\\" +
                            productId + "." + fileDetail.getFileName().split("\\.")[1])) {
                return Response.status(500).entity("Could not save image").build();
            }
        } catch (URISyntaxException | IOException e) {
            return Response.status(500).build();
        }
        return Response.status(200).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Update an existing product")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no product with the given id")
    })
    public Response updateProduct(
            @PathParam("id") int id,
            @ApiParam(name = "Product", value = "The updated product", required = true) Product product) {
        if (!productsService.updateProduct(id, product)) {
            return Response.status(404).build();
        }

        return Response.status(200).entity(product).build();
    }

    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Remove an existing product")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no product with the given id")
    })
    public Response removeProduct(@PathParam("id") int id) {
        if (!productsService.removeProduct(id)) {
            return Response.status(404).build();
        }
        return Response.status(200).build();
    }

    @GET
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Search product", response = Product.class)
    public Response searchProduct() {
        Product product = productsService.searchProduct();
        return Response.ok(product).build();
    }
}
